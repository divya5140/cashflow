package com.hcl.cashflow.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.cashflow.dto.CashFlow;
import com.hcl.cashflow.dto.CashFlowDto;
import com.hcl.cashflow.dto.Frequency;
import com.hcl.cashflow.dto.TransactionType;

@ExtendWith(SpringExtension.class)
public class CashFlowServiceImplTest {
	
	@InjectMocks
    private CashFlowServiceImpl cashFlowService;
	
	
    CashFlowDto cashFlowDto;

	 @Test
	    void testManageCashFlowWithNullReferenceDate() {
		 cashFlowDto=new CashFlowDto();
	        cashFlowDto.setInitialBalance(1000.0);
	        cashFlowDto.setReferenceDate(null);
	     
	        List<CashFlow> cashFlows = new ArrayList<>();
	        cashFlows.add(new CashFlow( TransactionType.CREDIT,Frequency.DAILY,100.0,LocalDate.of(2024, 1, 1)));
	        cashFlowDto.setCashflow(cashFlows);
	        IllegalArgumentException exception=assertThrows(IllegalArgumentException.class,()->{
	        	 cashFlowService.manageCashFlow(cashFlowDto);
	        });
	       
	        assertEquals("Reference date is null", exception.getMessage());
	    }

   

}



