package com.hcl.cashflow.dto;

import java.time.LocalDate;


public class CashFlow {

	TransactionType transactionType;
	Frequency frequency;
	Double amount;
	LocalDate startDate;
	
	public TransactionType getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
	public Frequency getFrequency() {
		return frequency;
	}
	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public LocalDate getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	public CashFlow(TransactionType transactionType, Frequency frequency, Double amount, LocalDate startDate) {
		super();
		this.transactionType = transactionType;
		this.frequency = frequency;
		this.amount = amount;
		this.startDate = startDate;
	}
	
}
