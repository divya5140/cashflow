package com.hcl.cashflow.dto;

public enum Frequency {

	DAILY,WEEKLY,MONTHLY
}
