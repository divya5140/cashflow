package com.hcl.cashflow.dto;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class DayTransaction {

	TransactionType transactionType;
	Frequency frequency;
	LocalDate date;
	Double amount;
	
}
