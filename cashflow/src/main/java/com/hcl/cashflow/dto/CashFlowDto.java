package com.hcl.cashflow.dto;

import java.time.LocalDate;
import java.util.List;


public class CashFlowDto {

	
	public Double initialBalance;
	public LocalDate referenceDate;
	public List<CashFlow> cashflow;
	public Double getInitialBalance() {
		return initialBalance;
	}
	public void setInitialBalance(Double initialBalance) {
		this.initialBalance = initialBalance;
	}
	public LocalDate getReferenceDate() {
		return referenceDate;
	}
	public void setReferenceDate(LocalDate referenceDate) {
		this.referenceDate = referenceDate;
	}
	public List<CashFlow> getCashflow() {
		return cashflow;
	}
	public void setCashflow(List<CashFlow> cashflow) {
		this.cashflow = cashflow;
	}
	
}
