package com.hcl.cashflow.service.implementation;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.hcl.cashflow.dto.CashFlow;
import com.hcl.cashflow.dto.CashFlowDto;
import com.hcl.cashflow.dto.Frequency;
import com.hcl.cashflow.dto.TransactionType;
import com.hcl.cashflow.service.CashFlowService;

@Service
public class CashFlowServiceImpl implements CashFlowService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public String manageCashFlow(CashFlowDto cashFlowDto) {

		

		LocalDate dateTracker = cashFlowDto.getReferenceDate();
		if(dateTracker==null)
		{
			throw new IllegalArgumentException("Reference date is null");
		}

		Double updatedBalance;

		while (cashFlowDto.getInitialBalance() >= 0) {

			for (CashFlow cashFlow : cashFlowDto.getCashflow()) {
				if (cashFlowDto.getInitialBalance() ==0) {

					return dateTracker.toString();
				}
				
				if (cashFlowDto.getInitialBalance() < 0) {

					return dateTracker.toString();
				}
				

				
				if (cashFlow.getStartDate().isBefore(dateTracker) && cashFlow.getFrequency().equals(Frequency.DAILY)) {
					cashFlow.setStartDate(dateTracker);
				}
				while (cashFlow.getStartDate().isBefore(dateTracker) && cashFlow.getFrequency().equals(Frequency.WEEKLY)  ) {
					cashFlow.setStartDate(cashFlow.getStartDate().plusWeeks(1));
				}
				while(cashFlow.getStartDate().isBefore(dateTracker) && cashFlow.getFrequency().equals(Frequency.MONTHLY)) {
	                 cashFlow.setStartDate(cashFlow.getStartDate().plusMonths(1));		
				
				}
				if (cashFlow.getStartDate().equals(dateTracker)) {

					switch (cashFlow.getFrequency()) {

					case DAILY:

						cashFlow.setStartDate(cashFlow.getStartDate().plusDays(1));

						break;

					case WEEKLY:

						cashFlow.setStartDate(cashFlow.getStartDate().plusWeeks(1));

						break;

					case MONTHLY:

						cashFlow.setStartDate(cashFlow.getStartDate().plusMonths(1));

						break;

					default:

						throw new IllegalArgumentException("Unexpected value: " + cashFlow.getFrequency());

					}

					if (cashFlow.getTransactionType().equals(TransactionType.CREDIT))

						updatedBalance = cashFlowDto.getInitialBalance() + cashFlow.getAmount();

					else

						updatedBalance = cashFlowDto.getInitialBalance() - cashFlow.getAmount();

					cashFlowDto.setInitialBalance(updatedBalance);

					log.info("Balance: " + cashFlowDto.getInitialBalance() + "Date: " + dateTracker

							+ "Transaction Amount:" + cashFlow.getAmount());

				}

			}
			if (cashFlowDto.getInitialBalance() < 0) {

				return dateTracker.toString();
			}

			dateTracker = dateTracker.plusDays(1);

		}
		log.error("Transaction error ");
		return "Transaction Error";

	}

}