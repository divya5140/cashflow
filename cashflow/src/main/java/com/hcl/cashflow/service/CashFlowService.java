package com.hcl.cashflow.service;

import com.hcl.cashflow.dto.CashFlowDto;

public interface CashFlowService {

	String manageCashFlow(CashFlowDto cashFlowDto);
}
