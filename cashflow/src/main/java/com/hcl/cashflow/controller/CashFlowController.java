package com.hcl.cashflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.cashflow.dto.CashFlowDto;
import com.hcl.cashflow.service.CashFlowService;

@RestController
@RequestMapping("/")
public class CashFlowController {
	
	@Autowired
	CashFlowService cashFlowServiceImpl;
	
	
	@PostMapping("/cashflow")
	public ResponseEntity<String> cashFlow(@RequestBody CashFlowDto cashFlowDto) {
		return new ResponseEntity<>(cashFlowServiceImpl.manageCashFlow(cashFlowDto), HttpStatus.CREATED);
	}
	
}
